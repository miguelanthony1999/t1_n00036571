﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T1_N00036571.Servicios
{
	public class Jugador
	{
		public int Id { get; set; }
		public String Nombre { get; set; }
		public List<Puntaje> Punto { get; set; }
		public int Puntaje { get; set; }

	}
}
