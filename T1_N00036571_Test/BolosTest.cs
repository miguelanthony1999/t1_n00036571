using NUnit.Framework;
using T1_N00036571;
using T1_N00036571.Servicios;


namespace T1_N00036571_Test
{
	public class BolosTests
	{
        //Pruebas Caso registrar jugadores
        [Test]
        public void Caso1Jugadores()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Miguel" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(1, resultado);
        }
        [Test]
        public void Caso3Jugadores()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarJugador(new Jugador { Id = 2, Nombre = "Erick" });
            bolos.RegistrarJugador(new Jugador { Id = 3, Nombre = "Fabiana" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(3, resultado);
        }
        [Test]
        public void Caso0Jugadores()
        {
            var bolos = new Bolos();
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(0, resultado);
        }

        // Pruebas Caso lanzamientos
        [Test]
        public void Caso1()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(21, resultado);
        }
        [Test]
        public void Caso2()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 3 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 6 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(17, resultado);
        }
        [Test]
        public void Caso3()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(21, resultado);
        }
        [Test]
        public void Caso4()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });

            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 4 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(28, resultado);
        }
        [Test]
        public void Caso5()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });

            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 9, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 5 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(35, resultado);
        }


        /// <summary>
        /// /////////////////////////////////////////////////////
        /// </summary>
        [Test]
        public void Caso6()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });

            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 9, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 6 });

            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(33, resultado);
        }
        [Test]
        public void Caso7()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10});
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });

            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(64, resultado);
        }
        [Test]
        public void Caso8()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 7 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 8 });

            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(61, resultado);
        }
        [Test]
        public void Caso9()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 0});
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 7 });

            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(22, resultado);
        }
        [Test]
        public void Caso10()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 0 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(81, resultado);
        }
        [Test]
        public void Caso11()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 9 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 6 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(81, resultado);
        }
        [Test]
        public void Caso12()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 9 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 2 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(56, resultado);
        }
        [Test]
        public void Caso13()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 1});
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(112, resultado);
        }
        [Test]
        public void Caso14()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 0});
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(56, resultado);
        }
        [Test]
        public void Caso15()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 7, lanzar2 = 3 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 4 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(55, resultado);
        }
        [Test]
        public void Caso16()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Anthony" });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 8 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 8, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 0 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(54, resultado);
        }

    }
}